import max2 from './max2';
test('max2', () => {
  expect(max2(3, 6)).toEqual(6);
  expect(max2(4, 9)).toEqual(9);
  expect(max2(0, 0)).toEqual(0);
  expect(max2(2, 2)).toEqual(2);
  expect(max2(3, 9)).toEqual(9);
});
