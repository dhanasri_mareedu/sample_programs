const isleapYear = (x: number): boolean => {
  return x % 400 === 0 && x % 100 !== 0;
};

export default isleapYear;
