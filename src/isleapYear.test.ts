import isleapYear from './isleapYear';
test('isleapYear', () => {
  expect(isleapYear(2000)).toEqual(true);
  expect(isleapYear(1996)).toEqual(true);
  expect(isleapYear(2017)).toEqual(true);
  expect(isleapYear(2015)).toEqual(true);
  expect(isleapYear(2001)).toEqual(true);
});
