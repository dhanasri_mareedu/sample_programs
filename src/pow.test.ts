import power from './pow';

test('power', () => {
  expect(power(1, 2)).toEqual(1);
  expect(power(2, 2)).toEqual(4);
  expect(power(1, 100)).toEqual(1);
  expect(power(100, 0)).toEqual(100);
  expect(power(0, 0)).toEqual(0);
  expect(power(2, 100)).toEqual(100);
});
