const squareAll = (arr: number[]): number[] => {
  const temp = [];
  for (let i = 0; i < arr.length; i++) {
    temp.push(arr[i] * arr[i]);
  }
  return temp;
};

export default squareAll;
