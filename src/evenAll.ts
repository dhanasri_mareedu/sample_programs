const evenAll = (arr: number[]): number[] => {
  const res: number[] = [];
  for (let i = 0; i <= arr.length; i++) {
    if (arr[i] % 2 === 0) {
      res.push(arr[i]);
    }
  }
  return res;
};

export default evenAll;
