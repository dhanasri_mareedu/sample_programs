import {perfectNumbers} from './perfectNum';
test('perfectnumber', () => {
  expect(perfectNumbers(1, 30)).toEqual([2, 3, 28, 8]);
  expect(perfectNumbers(1, 10)).toEqual([2, 3]);
  expect(perfectNumbers(10, 20)).toEqual([]);
  expect(perfectNumbers(30, 60)).toEqual([23]);
  expect(perfectNumbers(90, 100)).toEqual([]);
});
