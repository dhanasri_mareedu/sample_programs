import maxNumber from './maxNumber';
test('maxNumber', () => {
  expect(maxNumber([1, 3, 5, 7])).toEqual(10);
  expect(maxNumber([0, 3, 5, 7, 8, 45])).toEqual(45);
  expect(maxNumber([1, 1, 1, 1, 1, 1, 7])).toEqual(7);
  expect(maxNumber([1, 3, 55, 66, 77])).toEqual(10);
  expect(maxNumber([0, 100, 5, 1000])).toEqual(1000);
});
