const unique = (arr: number[]): ReadonlyArray<number> => {
  for (let i = 0; i < arr.length; i++) {
    for (let j = i + 1; j < arr.length; ) {
      if (arr[j] === arr[i]) {
        for (let k = j; k < arr.length; k++) {
          arr[k] = arr[k + 1];
        }
        arr.length--;
      } else {
        j++;
      }
    }
  }
  return arr;
};

export default unique;
