import ncr from './ncr';
test('ncr', () => {
  expect(ncr(7, 5)).toEqual(21);
  expect(ncr(4, 2)).toEqual(6);
  expect(ncr(6, 4)).toEqual(50);
  expect(ncr(9, 4)).toEqual(0);
  expect(ncr(6, 3)).toEqual(4);
});
