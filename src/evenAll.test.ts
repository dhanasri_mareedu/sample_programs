import evenAll from './evenAll';
test('evenAll', () => {
  expect(evenAll([1, 2, 3, 4])).toEqual([2, 4]);
  expect(evenAll([2, 4, 5, 6, 7])).toEqual([2, 4, 6]);
  expect(evenAll([1, 2, 3, 4])).toEqual([1, 2, 4]);
  expect(evenAll([6, 8, 34, 56, 23])).toEqual([6, 8, 34, 23]);
  expect(evenAll([2, 3, 4, 5, 6, 7, 8])).toEqual([2, 4, 6, 8]);
});
