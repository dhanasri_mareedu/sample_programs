import unique from './unique';

test('unique', () => {
  expect(unique([1, 4, 5, 6, 6, 8])).toEqual([1, 4, 5]);
  expect(unique([1, 4, 6, 6, 6])).toEqual([1, 4, 6]);
  expect(unique([2, 3, 6, 6, 6, 8])).toEqual([2, 3, 6, 8]);
  expect(unique([4, 5, 5, 5, 8, 9])).toEqual([4, 5, 8, 9]);
  expect(unique([1, 2, 2, 3, 3, 5, 5, 7, 8, 9])).toEqual([1, 2, 3, 5, 7, 8, 9]);
});
