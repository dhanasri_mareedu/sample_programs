import liesBetween from './liesBetween';
test('liesBetween', () => {
  expect(liesBetween(2, 4, 6)).toEqual(4);
  expect(liesBetween(2, 5, 9)).toEqual(4);
  expect(liesBetween(5, 8, 9)).toEqual(8);
  expect(liesBetween(4, 6, 7)).toEqual(4);
  expect(liesBetween(2, 2, 2)).toEqual(2);
});
