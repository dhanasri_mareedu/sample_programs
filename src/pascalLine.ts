import ncr from './ncr';
import range from './range';

const pascalLine = (n: number): number[] => range(0, n).map(r => ncr(n, r));

const pascalTriangle = (lines: number): ReadonlyArray<ReadonlyArray<number>> =>
  range(0, lines + 1).map(line => pascalLine(line));
console.log(pascalTriangle(3));
