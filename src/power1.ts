const power1 = (x: number, n: number): number => {
  let temp = 1;
  for (let i = 0; i < n; i++) {
    temp = temp * x;
  }
  return temp;
};

export default power1;
