import min2 from './min2';

test('min2', () => {
  expect(min2(3, 6)).toEqual(3);
  expect(min2(2, 8)).toEqual(8);
  expect(min2(2, 2)).toEqual(2);
  expect(min2(3, 5)).toEqual(3);
  expect(min2(6, 8)).toEqual(8);
});
