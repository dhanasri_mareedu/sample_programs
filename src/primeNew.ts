import range from './range';

const prime4 = (n: number): boolean => {
  for (let i = 2; i < n; i++) {
    if (n % i === 0) {
      return false;
    }
    return true;
  }
  if (n === 2) {
    return true;
  }
  if (n === 1) {
    return false;
  }
  return true;
};

const primeNumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop + 1).filter(prime4);
console.log(primeNumbers(10, 20));

export default primeNumbers;
