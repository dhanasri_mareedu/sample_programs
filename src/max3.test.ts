import max3 from './max3';

test('max3', () => {
  expect(max3(2, 3, 4)).toEqual(4);
  expect(max3(5, 6, 8)).toEqual(8);
  expect(max3(4, 6, 8)).toEqual(6);
  expect(max3(3, 5, 6)).toEqual(5);
  expect(max3(3, 6, 9)).toEqual(6);
});
