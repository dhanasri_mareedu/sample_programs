import odd from './odd';
test('odd', () => {
  expect(odd(3)).toEqual(true);
  expect(odd(4)).toEqual(false);
  expect(odd(6)).toEqual(true);
  expect(odd(0)).toEqual(true);
  expect(odd(7)).toEqual(false);
});
