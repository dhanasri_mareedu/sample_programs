const max2 = (x: number, y: number): number => {
  if (x > y) {
    return x;
  }

  return y;
};

export default max2;
