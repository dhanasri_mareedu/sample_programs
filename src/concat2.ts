const push = (arr: ReadonlyArray<number>, n: number): ReadonlyArray<number> => [
  ...arr,
  n,
];
const concat2 = (
  arr1: ReadonlyArray<number>,
  arr2: ReadonlyArray<number>
): ReadonlyArray<number> => {
  for (let i = 0; i < arr1.length; i++) {
    arr1.push(arr1[i], arr2[i]);
  }
  return arr1;
};
export default concat2;
