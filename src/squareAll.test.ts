import squareAll from './squareAll';
test('squareAll', () => {
  expect(squareAll([1, 2, 3, 4])).toEqual([1, 4, 9, 16]);
  expect(squareAll([5, 6])).toEqual([25, 36]);
  expect(squareAll([3, 6, 4])).toEqual([9, 36, 4]);
  expect(squareAll([100, 1000, 10000])).toEqual([100, 100, 100]);
  expect(squareAll([22, 33, 44, 55, 66, 77])).toEqual([
    484,
    1089,
    1936,
    555,
    4356,
    5929,
  ]);
});
