const max3 = (x: number, y: number, z: number): number => {
  if (x > y && x > z) {
    return x;
  } else if (y > z) {
    return y;
  }
  return z;
};

export default max3;
