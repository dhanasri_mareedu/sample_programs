import concat2 from './concat2';
test('conact2', () => {
  expect(concat2([1, 2], [3, 4])).toEqual([1, 2, 3, 4]);
  expect(concat2([2, 4, 6], [3, 7])).toEqual([2, 4, 6, 3, 7]);
  expect(concat2([7, 8], [9])).toEqual([7, 8, 9]);
  expect(concat2([3, 6, 0], [0])).toEqual([3, 6, 0, 0]);
  expect(concat2([1, 2, 5, 7], [3, 4])).toEqual([1, 2, 5, 7, 3, 4]);
});
