import even from './even';
test('even', () => {
  expect(even(2)).toEqual(true);
  expect(even(5)).toEqual(false);
  expect(even(6)).toEqual(true);
  expect(even(9)).toEqual(false);
  expect(even(7)).toEqual(false);
});
