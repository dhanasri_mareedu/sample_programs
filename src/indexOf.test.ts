import indexOf from './indexOf';
test('indexOf', () => {
  expect(indexOf([2, 4, 5, 6], 4)).toEqual(1);
  expect(indexOf([3, 5, 7], 5)).toEqual(1);
  expect(indexOf([2, 5, 8, 9, 0], 0)).toEqual(4);
  expect(indexOf([4, 6, 8, 2], 2)).toEqual(2);
  expect(indexOf([4, 3, 5, 6, 7, 8], 5)).toEqual(5);
});
