const splitAt = (
  arr: ReadonlyArray<number>,
  n: number
): [ReadonlyArray<number>, ReadonlyArray<number>] => {
  const ft: number[] = [];

  for (let i = 0; i < n; i++) {
    ft.push(arr[i]);
  }
  const sd: number[] = [];

  for (let i = n; i < sd.length; i++) {
    sd.push(arr[i]);
  }
  return [ft, sd];
};

export default splitAt;
