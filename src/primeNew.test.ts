import {primeNumbers} from './primeNew';
test('primeNumbers', () => {
  expect(primeNumbers(10, 20)).toEqual([11, 13, 17, 19]);
  expect(primeNumbers(30, 50)).toEqual([29, 31, 37, 43, 41]);
  expect(primeNumbers(40, 90)).toEqual([43, 47, 53, 71, 61, 67, 31, 37]);
  expect(primeNumbers(36, 88)).toEqual([71, 61, 67]);
  expect(primeNumbers(3, 33)).toEqual([3, 5, 7, 11, 13, 17, 23]);
});
