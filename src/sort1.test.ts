import sort1 from './sort1';
test('sort1', () => {
  expect(sort1([3, 4, 1, 9], 3)).toEqual([1, 3, 4, 9]);
  expect(sort1([2, 7, 9, 4], 4)).toEqual([2, 4, 7, 9]);
  expect(sort1([3, 6, 1, 9, 0], 4)).toEqual([0, 1, 3, 6, 9]);
  expect(sort1([2, 7, 9, 4], 4)).toEqual([2, 4, 7, 9]);
  expect(sort1([2, 7, 9, 4], 4)).toEqual([2, 4, 7, 9]);
});
