const even = (n: number): boolean => {
  if (n % 2 === 0) {
    return true;
  }
  return false;
};

export default even;
