const zip = (
  arr1: ReadonlyArray<number>,
  arr2: ReadonlyArray<number>
): ReadonlyArray<[number, number]> => {
  const arr3: number[] = [];
  for (let i = 0; i <= arr1.length; i++) {
    arr3.push(arr1[i], arr2[i]);
  }
  return arr3;
};

export default zip;
