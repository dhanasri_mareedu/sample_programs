import range from './range';

const perfect = (n: number): boolean => {
  let sum = 0;
  for (let i = 1; i <= n / 2; i++) {
    if (n % i === 0) {
      sum = sum + i;
    }
    if (sum === n && sum !== 0) {
      return true;
    }
  }
  return false;
};

const perfectNumbers = (start: number, stop: number): ReadonlyArray<number> =>
  range(start, stop).filter(perfect);
console.log(perfectNumbers(10, 100));

export {perfect, perfectNumbers};
