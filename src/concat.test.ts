import concat from './concat';
test('concat', () => {
  expect(concat([1, 2, 3], [4, 5, 6])).toEqual([1, 2, 3, 4, 5, 6]);
  expect(concat([1, 2, 3], [5, 6])).toEqual([1, 2, 3, 4, 6]);
  expect(concat([6, 7, 8], [])).toEqual([6, 7, 8]);
  expect(concat([7, 8], [5, 4, 3])).toEqual([7, 8, 5, 4, 3]);
  expect(concat([5, 6, 8], [9, 9, 9])).toEqual([5, 6, 8, 9, 9, 9, 9]);
  expect(concat([3, 4, 6], [3, 4, 6])).toEqual([3, 4, 6, 3, 6, 4]);
});
