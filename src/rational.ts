import gcd from './gcd';

class Rational {
  readonly numerator: number;
  readonly denominator: number;
  constructor(numerator: number, denominator: number) {
    const g = gcd(numerator, denominator);
    this.numerator = numerator / g;
    this.denominator = denominator / g;
  }
  add(that: Rational): Rational {
    const numerator =
      this.numerator * that.denominator + this.denominator * that.numerator;
    const denominator = this.denominator * that.denominator;
    const result = new Rational(numerator, denominator);
    return result;
  }

  substract(that: Rational): Rational {
    const numerator =
      this.numerator * that.denominator - this.denominator * that.numerator;
    const denominator = this.denominator * that.denominator;
    const result = new Rational(numerator, denominator);
    return result;
  }
  multiply(that: Rational): Rational {
    const numerator =
      this.numerator * that.denominator * (this.denominator * that.numerator);
    const denominator =
      this.denominator * that.numerator * (this.numerator + that.denominator);
    const result = new Rational(numerator, denominator);
    return result;
  }
  divide(that: Rational): Rational {
    const numerator =
      this.numerator * that.denominator / (this.denominator * that.numerator);
    const denominator = this.denominator / that.denominator;
    const result = new Rational(numerator, denominator);
    return result;
  }
  compare(a: Rational): number {
    const b = this.numerator * a.denominator > this.denominator * a.numerator;
    if (b === 0) {
      return 0;
    }
    if (b > 0) {
      return 1;
    }
    if (b < 0) {
      return -1;
    }
  }
}
const r1 = new Rational(1, 2);
const r2 = new Rational(1, 4);
const r3 = r1.add(r2);
const r4 = r1.substract(r2);
const r5 = r1.multiply(r2);
const r6 = r1.divide(r2);
const r7 = r1.divide(r2);
console.log(r3.numerator, r3.denominator);
console.log(r4.numerator, r4.denominator);
console.log(r5.numerator, r5.denominator);
console.log(r6.numerator, r6.denominator);
console.log(r7);
