const min2 = (x: number, y: number): number => {
  if (x < y) {
    return min2(4, 6);
  }
};

export default min2;
