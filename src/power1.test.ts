import power1 from './power1';
test('power1', () => {
  expect(power1(2, 2)).toEqual(4);
  expect(power1(3, 2)).toEqual(9);
  expect(power1(10, 2)).toEqual(100);
  expect(power1(6, 2)).toEqual(36);
  expect(power1(1, 2)).toEqual(1);
});
