const liesBetween = (x: number, y: number, n: number): number => {
  if (x < n && n < y) {
    return n;
  }
  return -1;
};

export default liesBetween;
