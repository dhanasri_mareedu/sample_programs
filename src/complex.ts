class Complex {
  readonly rl: number;
  readonly img: number;
  constructor(rl: number, img: number) {
    this.rl = rl;
    this.img = img;
  }
  add(a: number, b: number): string {
    const rl = this.rl + a;
    const img = this.img + b;
    return '' + rl + '+ ' + img + 'i';
  }

  substract(a: number, b: number): string {
    const rl = this.rl - a;
    const img = this.img - b;
    return '' + rl + '' + img + 'i';
  }
  multiply(a: number, b: number): string {
    const rl = this.rl * a - a * b;
    const img = b * this.rl + a * this.img;
    return '' + rl + '+' + img + 'i';
  }
  divide(a: number, b: number): string {
    const rl = this.rl / a;
    const img = this.img / b;
    return ' ' + rl + '+' + img + 'i';
  }
  compare(c: number) {
    if (this.rl === c) {
      return 0;
    }
    if (this.rl > c) {
      return 1;
    }
    if (this.rl < c) {
      return -1;
    }
  }
}
const z1 = new Complex(1, 2);
const z3 = z1.add(2, 3);
const z4 = z1.substract(2, 3);
const z5 = z1.multiply(2, 3);
const z6 = z1.divide(2, 3);
const z7 = z1.compare(2);
console.log(z3);
console.log(z4);
console.log(z5);
console.log(z6);
console.log(z7);
