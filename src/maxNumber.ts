const maxNumber = (arr: ReadonlyArray<number>): number =>
  arr.reduce((x, y) => (x > y ? x : y));
console.log(maxNumber([1, 2, 6, 8, 4, 6, 38, 77, 45, 99]));

export default maxNumber;
