import arrLargest from './arrLargest';
test('arrLargest', () => {
  expect(arrLargest([2, 3, 4, 5, 6])).toEqual(6);
  expect(arrLargest([3, 4, 5, 6])).toEqual(5);
  expect(arrLargest([4, 6, 7, 8, 4])).toEqual(8);
  expect(arrLargest([3, 5, 6, 2, 1])).toEqual(6);
  expect(arrLargest([4, 6, 7, 8, 3, 2])).toEqual(8);
});
