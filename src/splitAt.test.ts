import splitAt from './splitAt';
test('splitAt', () => {
  expect(splitAt([1, 2, 3, 4, 5, 6], 2)).toEqual([[1, 2, 3], [4, 5, 6]]);
  expect(splitAt([1, 2, 3, 4], 2)).toEqual([[1, 2], [3, 4]]);
  expect(splitAt([1, 2], 1)).toEqual([[1], [2]]);
  expect(splitAt([1, 2, 3, 4, 5, 6, 7, 8], 4)).toEqual([
    [1, 2],
    [3, 4],
    [5, 6],
    [7, 8],
  ]);
  expect(splitAt([1, 2, 3, 4, 5, 6], 3)).toEqual([[1, 2], [3, 4], [5, 6]]);
});
